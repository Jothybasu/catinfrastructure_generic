﻿-- Create the CAT DB owner
CREATE USER cat_admin WITH PASSWORD 'changeme';

-- Create the Database cat_db and give cat_admin full access rights
CREATE DATABASE cat_db
  WITH OWNER = cat_admin
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;
GRANT ALL ON DATABASE cat_db TO cat_admin;